import os
import subprocess
import time
import sys
import argparse
import requests
import progressbar

FLAGS = None

root_folder = os.path.dirname(os.path.abspath(__file__))
download_folder = os.path.join(root_folder, 'src', 'keras_yolo3')


def downloadIfNeeded(pathWithFileName, url):
    needFile = True
    f = None
    try:
        f = open(pathWithFileName)
        needFile = False
    except IOError:
        pass
    finally:
        if f is not None:
            f.close()
    if needFile:
        r = requests.get(url, stream=True)
        f = open(pathWithFileName, 'wb')
        file_size = int(r.headers.get('content-length'))
        chunk = 100
        num_bars = file_size // chunk
        bar = progressbar.ProgressBar(maxval=num_bars).start()
        i = 0
        for chunk in r.iter_content(chunk):
            f.write(chunk)
            bar.update(i)
            i += 1
        f.close()
    else:
        print("Already downloaded, skipping download, of file:", pathWithFileName)


if __name__ == '__main__':
    # Delete all default flags
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    '''
    Command line options
    '''
    parser.add_argument(
        "--download_folder",
        type=str,
        default=download_folder,
        help="Folder to download weights to. Default is " + download_folder
    )
    parser.add_argument(
        "--is_tiny",
        default=False,
        action="store_true",
        help="Download and convert the tiny Yolo version. Default is False.",
    )

    FLAGS = parser.parse_args()

    if FLAGS.is_tiny:
        weights_filename = "yolov3-tiny.weights"
    else:
        weights_filename = "yolov3.weights"

    url = "https://pjreddie.com/media/files/" + weights_filename

    downloadIfNeeded(
        os.path.join(download_folder, weights_filename),
        url
    )

    if FLAGS.is_tiny:
        call_string = "python3 convert.py yolov3-tiny.cfg yolov3-tiny.weights yolo-tiny.h5"
    else:
        call_string = "python3 convert.py yolov3.cfg yolov3.weights yolo.h5"

    subprocess.call(call_string, shell=True, cwd=download_folder)
